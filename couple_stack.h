/*******************************************************
Name .......... : couple_stack.h
Goal .......... : Header file for the couple_stack.c stack structure.
Author ........ : Alex Coudray
Date .......... : 11/01/2018    (format MM/DD/YY)
********************************************************/

#ifndef COUPLE_STACK_H
#define COUPLE_STACK_H

typedef struct couple_stack couple_stack_t;
// Constructor
couple_stack_t* make_stack(int max_size);
// Destructor
void free_stack(couple_stack_t* p);
// Methods
couple_t* top_of_stack(couple_stack_t* p); // Returns the top of the stack
couple_t* extract_top(couple_stack_t* p); // Returns the top of the stack and delete it from the stack
void add_to_stack(couple_stack_t* p, couple_t* element); // Add an element to the stack
int is_empty(couple_stack_t* p); // Returns if the stack is empty
#endif /* COUPLE_STACK_H */
