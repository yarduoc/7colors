/*******************************************************
Name .......... : couple.h
Goal .......... : Header file for the couple.c couple structure
Author ........ : Alex Coudray
Date .......... : 11/01/2018    (format MM/DD/YY)
********************************************************/

#ifndef COUPLE_H
#define COUPLE_H

typedef struct couple couple_t;
// Constructor
couple_t* make_couple(int x, int y);
// Destructor
void free_couple();
// Getters for x and y attributes
int get_x( couple_t*);
int get_y( couple_t*);

#endif /* COUPLE_H */
