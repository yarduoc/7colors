/*******************************************************
Name .......... : player.c
Goal .......... : Giving a structure for player
Author ........ : Maxime Cauté
Date .......... : 23/10/2018    (format MM/DD/YY)
********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "game_board.h"
#include "player.h"

#define NB_COLORS 7

struct player
{
  int number;
  char color;
  int starting_x,starting_y;
  int behavior;
};

player_t* player_create(int number, char color, int x, int y, int behavior)
{
  player_t* res= malloc(sizeof(player_t));
  res -> number = number;
  res -> color = color;
  res -> starting_x = x;
  res -> starting_y = y;
  res -> behavior = behavior;
  return res;
}

void player_free(player_t* p)
{
  free(p);
}

char get_color(player_t* p)
{
  return (p -> color);
}

int get_starting_x(player_t* p)
{
  return(p -> starting_x);
}

int get_starting_y(player_t* p)
{
  return(p -> starting_y);
}

int player_explore(game_board_t* board, player_t* p, char color){
  int x = get_starting_x(p);
  int y = get_starting_y(p);
  char pc = get_color(p);
  return explore(board, x, y, pc, color);
}

int player_get_perimeter_gain( game_board_t* board,
                              player_t* p,
                              char color,
                              player_t** players  )
{
  int x = get_starting_x(p);
  int y = get_starting_y(p);
  char player_color = get_color(p);
  char opponent_color;
  if (players[0]==p)
    opponent_color=get_color(players[1]);
  else
    opponent_color=get_color(players[0]);
  return (  get_perimeter(board, x, y, player_color, color, opponent_color)-
            get_perimeter(board, x, y, player_color, player_color, opponent_color) );
}

// Actions

char human_choice()
{
  char color;

  do {
    printf("What color would you like to play ?\n");
    scanf("%c",&color);
    int c;
    while ((c = getchar()) != '\n' && c != EOF) { };
  } while (color-65< 0 || color-65 >= NB_COLORS);

  return color;
}

char random_useful_choice(game_board_t* board, player_t* p){
  printf("Random playin'\n");
  char color;
  srand((unsigned) time(NULL));
  int tested[NB_COLORS]={0};
  int nb_tests = 0;

  do
  {
    color =(char)(rand()%NB_COLORS);


    int x = get_starting_x(p);
    int y = get_starting_y(p);
    char pc = get_color(p);

    int potential_gain=0;

    if(tested[(int)color]== 0)
      potential_gain=explore(board, x, y, pc, 65+color);


    if(tested[(int)color]== 0 && potential_gain<=0) //Explore returns the number of cells that would be added (could limit to returning if at least one would be added )
    {
      tested[(int)color]=1;

      nb_tests++;
    }

  } while (tested[(int)color]==1 && nb_tests <NB_COLORS);

  return color+65;
}

char panic(game_board_t* board, player_t* p)
{
  printf("!! PANICKING !!\n");
  return random_useful_choice(board,p);
}

char greedy_choice(game_board_t* board, player_t* p)
{
  printf("Greedy playin'\n");
  int optimal_gain=0;
  char optimal_color=0;

  for(int c=0;c<NB_COLORS;c++)
  {
    int gain = player_explore(board,p,65+c);
    if (gain>=optimal_gain)
    {
      optimal_gain=gain;
      optimal_color=c;
    }
  }
  if (optimal_gain==0)
    return panic(board,p);
  else
    return optimal_color+65;

}



char hegemonic_choice(game_board_t* board, player_t* p, player_t** players)
{
  printf("Hegemonic playin'\n");
  int optimal_gain=1;
  char optimal_color='$';

  for(int c=0;c<NB_COLORS;c++)
  {
    int influence_gain = (player_get_perimeter_gain(board,p,65+c, players));
    if (influence_gain>=optimal_gain)
    {
      //printf("I evaluated %c as a better option ! Gain would be %d\n", c+65, influence_gain);
      optimal_gain=influence_gain;
      optimal_color=c;
    }
  }

  if (optimal_color=='$')
    return panic(board,p);
  else
    return optimal_color+65;
}

/*
int abs(int x)
{
  if (x>=0) return -x; else return x;
}


char absolute_hegemonic_choice(game_board_t* board, player_t* p, player_t** players)
{
  printf("Hegemonic playin'\n");
  int optimal_gain=1;
  char optimal_color='$';

  for(int c=0;c<NB_COLORS;c++)
  {
    int influence_gain = abs(player_get_perimeter_gain(board,p,65+c, players));
    if (influence_gain>=optimal_gain)
    {
      //printf("I evaluated %c as a better option ! Gain would be %d\n", c+65, influence_gain);
      optimal_gain=influence_gain;
      optimal_color=c;
    }
  }

  if (optimal_color=='$')
    return panic(board,p);
  else
    return optimal_color+65;
}*/

char get_greedy_advice(game_board_t* board, player_t* p)
{
  printf("Asking Greedy for advice...");
  return greedy_choice(board, p);
}

char hybrid_choice(game_board_t* board, player_t* p, player_t** players)
{
  printf("Hegemonic playin'\n");
  int optimal_gain=1;
  char optimal_color='$';

  for(int c=0;c<NB_COLORS;c++)
  {
    int influence_gain = (player_get_perimeter_gain(board,p,65+c, players));
    if (influence_gain>=optimal_gain)
    {
      //printf("I evaluated %c as a better option ! Gain would be %d\n", c+65, influence_gain);
      optimal_gain=influence_gain;
      optimal_color=c;
    }
  }

  if (optimal_color=='$')
    return get_greedy_advice(board,p);
  else
    return optimal_color+65;
}



char select_color(game_board_t* board, player_t* p, player_t** players)
{
  char color=0;
  switch (p -> behavior){

    case 0: //human player
      color = human_choice();
      break;

    case 1://improved random AI
      color= random_useful_choice(board,p);
      break;

    case 2: // greedy algorithm
      color = greedy_choice(board,p);
      break;

    case 3: // hegemonic algorithm
      color = hegemonic_choice(board, p, players);
      break;

    case 4: // hybrid algorithm
      color = hybrid_choice(board,p,players);
      break;

    default: printf("Behavior Exception !");

  }
  printf("Couleur : %c\n",color);
  return color;
}




void play(game_board_t* board, player_t* player, player_t** players)
{
  print_board(board);
  char target_color = select_color(board, player ,players);
  int x = get_starting_x(player);
  int y = get_starting_y(player);
  explore_replace(board, x, y, get_color(player), target_color);
}
