/*******************************************************
Name .......... : game_board.c
Goal .......... : Giving a game_board structure to
Author ........ : Alex Coudray
Date .......... : 11/01/2018    (format MM/DD/YY)
********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "couple.h"
#include "couple_stack.h"
#include "game_board.h"

// Structure definition
struct game_board
{
    int board_size, size_of_player_1, size_of_player_2;
    char* board_data;
};

// Constructor
game_board_t* make_board( int size)
{
    game_board_t* board = malloc(sizeof(game_board_t));
    board -> board_size = size;
    board -> size_of_player_1 = 1;
    board -> size_of_player_2 = 1;
    board -> board_data = malloc(sizeof(char)*size*size);
    return board;
}

// Destructor
void free_board( game_board_t* board)
{
    free(board -> board_data);
    free(board);
}

// Returns the percentage of board owned by player 1 or 2
double get_percentage( game_board_t* board, int player_id)
{
    double b_size = (double)(board -> board_size);
    double size_1 = (double)(board -> size_of_player_1);
    double size_2 = (double)(board -> size_of_player_2);
    if (player_id == 1)
    {
        return (100. * size_1 / (b_size * b_size));
    }
    else if(player_id == 2)
    {
        return (100. * size_2 / (b_size * b_size));
    }
    else
    {
        return 0;
    }
}

// Returns the color of a given board cell
char get_cell( game_board_t* board, int x, int y)
{
    return (board -> board_data)[y * (board -> board_size) + x];
}

// Changes the color of a given board cell
void set_cell( game_board_t* board, int x, int y, char color)
{
    (board -> board_data)[y * (board -> board_size) + x] = color;
}

/*
Prints a char with a certain background
The instruction \033[030;XXm calls for ANSI escape sequence for a black text
over a background which color depends on XX value.
The instruction \033[0m resets formating parameters.
*/
void print_colored_char( char c)
{
    switch(c)
    {
        case 'A'  : printf("\033[100;030m A \033[0m");
        break; // 100 means bright black background

        case 'B'  : printf("\033[030;42m B \033[0m");
        break; // 42 means green background

        case 'C'  : printf("\033[030;43m C \033[0m");
        break; // 43 means yellow background

        case 'D'  : printf("\033[030;45m D \033[0m");
        break;// 45 means magenta background

        case 'E'  : printf("\033[030;46m E \033[0m");
        break; // 46 means cyan background

        case 'F'  : printf("\033[030;47m F \033[0m");
        break; // 47 means white background

        case 'G'  : printf("\033[030;105m G \033[0m");
        break; // 105 means bright magenta background

        case 'o'  : printf("\033[030;5;44m o \033[0m");
        break; // 44 means blue background, 5 makes the text blink

        case 'x'  : printf("\033[030;41m x \033[0m");
        break; // 41 means red background

        default : printf("%c",c );
    }
}

// Prints the current state of the board on screen
void print_board( game_board_t* board)
{
    printf(" \033[030;44m Player 1 (o) : %d cases (%lf%%) \033[0m \033[030;41m Player 2 (x) : %d cases (%lf%%)\033[0m \n",
           (board -> size_of_player_1), get_percentage(board,1),
           (board -> size_of_player_2), get_percentage(board,2) );
    int i, j;
    for (i = 0; i < (board -> board_size); i++) {
        for (j = 0; j < (board -> board_size); j++) {
            print_colored_char(get_cell( board, i, j));
        }
        printf("\n");
    }
}

// Generates a random board
game_board_t* generate_board( int size)
{
    srand((unsigned) time(NULL)); //seeding random generator
    game_board_t* board = make_board( size);
    int x, y;
    for (x = 0; x < (board -> board_size); x++)
    {
        for (y = 0; y < (board -> board_size); y++)
        {
            int color= rand()%7;
            set_cell(board, x, y, color+65);
        }
    }
    set_cell(board,(board -> board_size) - 1,0,'x');
    set_cell(board,0,(board -> board_size) - 1,'o');
    return board;
}

// Returns if a cell is on the board, given its coordinates
int is_in_board( game_board_t* board, int x, int y)
{
    int b_size = (board -> board_size);
    if(x < 0 || y < 0 || x >= b_size || y >= b_size)
        { return 0; }
    else
        { return 1; }
}

/*
Returns whether a cell is to be explored by an exploration of the board or not.
To be explored, a cell needs to be on the board, not to be already explored
and to be of the right color.
*/
int is_to_explore( game_board_t* board, int* seen, int x, int y,
                   char player_color, char target_color)
{
    return is_in_board( board, x, y)
           && !(seen[y * (board -> board_size) + x])
           && (  get_cell( board, x, y) == player_color
              || get_cell( board, x, y) == target_color );
}

/*
Returns whether a cell is to be explored by an exploration of the board or not.
To be explored, a cell needs to be on the board and not to be already explored.
All non accessibles cells are counted to define the perimeter of the zone.
*/

int is_to_explore_perimeter( game_board_t* board, int* seen, int x, int y,
                             char player_color, char target_color,
                             char opponent_color, int* n_cells)
{
    if( is_in_board( board, x, y)
       && !(seen[y * (board -> board_size) + x]) )
    {
        if ( get_cell( board, x, y) == player_color
          || get_cell( board, x, y) == target_color )
        {
              return 1;
        }
        else if ( get_cell( board, x, y) != opponent_color)
        {
            *n_cells = *n_cells + 1 ;
            seen[y * (board -> board_size) + x] = 1;
        }
    }
    return 0;
}

// Continue without modifying the board during its exploration
void explore_treatement( game_board_t* board, couple_stack_t* stack,
                         int* seen, int x, int y)
{
    add_to_stack(stack, make_couple( x, y));
    seen[y * (board -> board_size) + x] = 1;
}

// Modify the board during its exploration
void explore_replace_treatement( game_board_t* board, couple_stack_t* stack,
                                 int* seen, char player_color, int x, int y)
{
    explore_treatement( board, stack, seen, x, y);
    set_cell(board, x, y, player_color);
}


/*
Explore the board from the given coordinates, only going over cells matching
player color or target color. Also update the count of cells of each player's
color. During its exploration it replaces all cells of target_color by cells of
player_color.
*/
void explore_replace( game_board_t* board, int i, int j,
                      char player_color, char target_color)
{
    int b_size = (board -> board_size);
    int count_of_cells = 0;
    int* seen = malloc(b_size*b_size*(sizeof(int)));
    seen[i * b_size + j] = 1;
    couple_stack_t* stack = make_stack(b_size*b_size);
    add_to_stack( stack, make_couple(i,j));
    while(!is_empty(stack))
    {
        couple_t* top = extract_top(stack);
        int x = get_x( top);
        int y = get_y( top);
        free_couple(top);
        if (is_to_explore( board, seen, x + 1, y, player_color, target_color))
        {
            explore_replace_treatement(board, stack, seen, player_color, x+1, y);
            count_of_cells++;
        }
        if (is_to_explore( board, seen, x - 1, y, player_color, target_color))
        {
            explore_replace_treatement(board, stack, seen, player_color, x-1, y);
            count_of_cells++;
        }
        if (is_to_explore( board, seen, x, y + 1, player_color, target_color))
        {
            explore_replace_treatement(board, stack, seen, player_color, x, y+1);
            count_of_cells++;
        }
        if (is_to_explore( board, seen, x, y - 1, player_color, target_color))
        {
            explore_replace_treatement(board, stack, seen, player_color, x, y-1);
            count_of_cells++;
        }
    }

    //free(seen);
    //free_stack(stack);

    if(player_color == 'o')
    {
        board -> size_of_player_1 = count_of_cells;
    }
    else
    {
        board -> size_of_player_2 = count_of_cells;
    }

}

/*
Explore the board from the given coordinates, only going over cells matching
player color or target color. Returns the number of cells of target_color
reachable by the exploration.
*/
int explore( game_board_t* board, int i, int j,
             char player_color, char target_color)
{
    int b_size = (board -> board_size);
    int count_of_cells = 0;
    int* seen = malloc(b_size*b_size*(sizeof(int)));
    seen[i * b_size + j] = 1;
    couple_stack_t* stack = make_stack(b_size*b_size);
    add_to_stack( stack, make_couple(i,j));
    while(!is_empty(stack))
    {
        couple_t* top = extract_top(stack);
        int x = get_x( top);
        int y = get_y( top);
        free_couple(top);
        if (is_to_explore( board, seen, x + 1, y, player_color, target_color))
        {
            explore_treatement( board, stack, seen, (x + 1), y);
            count_of_cells++;
        }
        if (is_to_explore( board, seen, x - 1, y, player_color, target_color))
        {
            explore_treatement( board, stack, seen, (x - 1), y);
            count_of_cells++;
        }
        if (is_to_explore( board, seen, x, y + 1, player_color, target_color))
        {
            explore_treatement( board, stack, seen, x, (y + 1));
            count_of_cells++;
        }
        if (is_to_explore( board, seen, x, y - 1, player_color, target_color))
        {
            explore_treatement( board, stack, seen, x, (y - 1));
            count_of_cells++;
        }
    }

    //free(seen);
    //free_stack(stack);

    if(player_color == 'o')
    {
        return count_of_cells - (board -> size_of_player_1);
    }
    else
    {
        return count_of_cells - (board -> size_of_player_2);
    }

}


int get_perimeter( game_board_t* board, int i, int j,
                   char player_color, char target_color, char opponent_color)
{
    int b_size = (board -> board_size);
    int* count_of_cells = malloc(sizeof(int));
    *count_of_cells = 0;
    int* seen = malloc(b_size*b_size*(sizeof(int)));
    seen[i * b_size + j] = 1;
    couple_stack_t* stack = make_stack(b_size*b_size);
    add_to_stack( stack, make_couple(i,j));
    while(!is_empty(stack))
    {
        couple_t* top = extract_top(stack);
        int x = get_x( top);
        int y = get_y( top);
        //free_couple(top);
        if (is_to_explore_perimeter( board, seen, x + 1, y, player_color,
            target_color, opponent_color, count_of_cells))
        {
            explore_treatement( board, stack, seen, (x + 1), y);
        }
        if (is_to_explore_perimeter( board, seen, x - 1, y, player_color,
            target_color, opponent_color, count_of_cells))
        {
            explore_treatement( board, stack, seen, (x - 1), y);
        }
        if (is_to_explore_perimeter( board, seen, x, y + 1, player_color,
            target_color, opponent_color, count_of_cells))
        {
            explore_treatement( board, stack, seen, x, (y + 1));
        }
        if (is_to_explore_perimeter( board, seen, x, y - 1, player_color,
            target_color, opponent_color, count_of_cells))
        {
            explore_treatement( board, stack, seen, x, (y - 1));
        }
    }
    //free(count_of_cells);
    //free_stack(stack);
    //free(seen);
    return *count_of_cells;

}
