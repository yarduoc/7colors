/*******************************************************
Name .......... : player.h
Goal .......... : Header for player structure
Author ........ : Maxime Cauté
Date .......... : 23/10/2018    (format MM/DD/YY)
********************************************************/

#ifndef PLAYER_H
#define PLAYER_H
#include "game_board.h"

typedef struct player player_t;

player_t* player_create (int number, char color, int x, int y, int behavior);
void player_free(player_t* p);
char get_color(player_t* p);
int get_starting_x(player_t* p);
int get_starting_y(player_t* p);
void play(game_board_t* board, player_t* p, player_t** players);

#endif /*PLAYER_H */
