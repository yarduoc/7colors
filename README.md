# 7 Wonders

This game was develloped for the Arcsys class from the ENS Rennes in 2018.

## Build Status

This project is over. Final version was to be released by 11/09/2018 at 22h CEST.
The sources are given within the archive so feel free to make your own updates.

## Requirements

You only need a computer and all your wits to play this game. Even friends aren't required anymore with our awesome single player mode !
> **Note:** In order to recompile the sources you need **make** installed on your computer though.

## Build and play instructions

- Unzip the archive 7colors_source.zip
- Run `$ make` in the unziped directory
- Run `$ ./7colors`
- Have fun !

## Features of the game

This games features different game modes.
Feeling lonely ? Try the **Single Player** mode and face its 4 fearfull artificial intelligences.
Want to fool your friends and show them who's the best ? The **Multiplayer** mode is made for you !
Configure your nation and conquer all the 7 colors of the world !

### A.I. Featured :
- Random the crazy conqueror. His moves makes barely sense.
- Greedy the Sultan. He always try to take the most he can.
- Hegemonic the emperor. He will always want to have the most spread out empire.
- Hybrid the superintelligent hybrid being. He will make the best decisions of Greedy and Hegemonic to beat you !

Can you beat them all ?

## Credits

This project was develloped by **Alex Coudray** and **Maxime Cauté** based on an idea from Martin Quinson, teacher at the ENS Rennes.
You can also find 7 colors on [Gitlab](https://gitlab.com/yarduoc/7colors.git)
