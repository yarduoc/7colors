/*******************************************************
Name .......... : couple_stack.c
Goal .......... : Giving a stack structure for int couples elements.
Author ........ : Alex Coudray
Date .......... : 11/01/2018    (format MM/DD/YY)
********************************************************/

#include "couple.h"
#include "couple_stack.h"
#include <stdlib.h>
#include <stdio.h>

// Structure definition
struct couple_stack
{
    int current_top_of_stack;
    couple_t** stack_data;
};

// Constructor
couple_stack_t* make_stack(int max_size)
{
    couple_stack_t* p = malloc(sizeof(couple_stack_t));
    couple_t** data = malloc(max_size * sizeof(couple_t*));
    p -> current_top_of_stack = (-1);
    p -> stack_data = data;
    return p;
}

// Destructor
void free_stack(couple_stack_t* p)
{
    free(p -> stack_data);
    free(p);
}

// Returns the top element of the stack
couple_t* top_of_stack(couple_stack_t* p)
{
    return (p -> stack_data)[(p -> current_top_of_stack)];
}

// Returns the top element of the stack and delete it from the stack
couple_t* extract_top(couple_stack_t* p)
{
    (p -> current_top_of_stack) = (p -> current_top_of_stack) - 1;
    return (p -> stack_data)[(p -> current_top_of_stack) + 1];
}

// Add an element to the stack
void add_to_stack(couple_stack_t* p, couple_t* element)
{
    (p -> current_top_of_stack) = (p -> current_top_of_stack) + 1;
    (p -> stack_data)[(p -> current_top_of_stack)] = element;
}

// Returns whether stack is empty or not
int is_empty(couple_stack_t* p)
{
    return ((p -> current_top_of_stack) < 0);
}
