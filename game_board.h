/*******************************************************
Name .......... : game_board.h
Goal .......... : Header file for game_board structure
Author ........ : Alex Coudray
Date .......... : 11/01/2018    (format MM/DD/YY)
********************************************************/

#ifndef GAME_BOARD_H
#define GAME_BOARD_H

// Structure definition
typedef struct game_board game_board_t;
// Constructor and random initializer
game_board_t* make_board( int size);
game_board_t* generate_board( int size);
// Destructor
void free_board( game_board_t* board);
// Printer
void print_board( game_board_t* board);
// Exploration algorithms
int explore( game_board_t* board, int x, int y,
             char player_color, char target_color);
void explore_replace( game_board_t* board, int x, int y,
                      char player_color, char target_color);
int get_perimeter( game_board_t* board, int i, int j,
                   char player_color, char target_color, char opponent_color);
// Returns percentage of board controlled by player
double get_percentage( game_board_t* board, int player_id);
// Getter and Setter for a cell given its coordinates
char get_cell( game_board_t* board, int x, int y);
void set_cell( game_board_t* board, int x, int y, char color);
#endif /* GAME_BOARD_H */
