/*******************************************************
Name .......... : 7colors.c
Goal .......... : Main body of the 7colors game
Author ........ : Maxime Cauté
Date .......... : 23/10/2018    (format MM/DD/YY)
********************************************************/

#include <stdio.h>     /* printf */
#include <stdlib.h>
#include <time.h>
#include "player.h"
#include "game_board.h"


#define NB_COLORS 7

void free_players(player_t** players)
{
  player_free(players[0]);
  player_free(players[1]);
}

int end_game(game_board_t* board, player_t** players)
{
  if(get_percentage(board,1) >= 50.)
  {
      print_board(board);
      return 1;
  }
  else if (get_percentage(board,2) >= 50.)
  {
      print_board(board);
      return 2;
  }
  else
      return 0;
}

void fight(player_t** players)
{
    game_board_t* board = generate_board(20);
    int number_player=0;
    while(!end_game(board, players))
    {
        printf("\n");
        play(board, players[number_player], players);
        number_player=(number_player + 1) % 2;
    }
    free_board(board);
}

void turnament(player_t** players, int nb_fights, int* scores)
{

  int score_p1=0;
  int score_p2=0;
  game_board_t* board;

  for (int i=0;i<nb_fights;i++)
  {
    int result=0;

    board=generate_board(20);

    int number_player=0;
    while(!result)
    {
        result = end_game(board, players);
        //printf("Result : %d, %fl, %fl",result, get_percentage(board,1), get_percentage(board,2));
        play(board, players[number_player], players);
        number_player=(number_player + 1) % 2;
    }

    if(result==1)
      score_p1++;
    else
      score_p2++;

    free(board);
  }

  scores[0] = score_p1;
  scores[1] = score_p2;

}

void quick_match_mode()
{
  printf("Fight mode !\n");
  printf("What player do you want to play with ?\n");
  printf("0/Human player, I am very fine with my friends/myself\n");
  printf("1/Random, his craziness suits me\n");
  printf("2/Greedy, let's see who'll get the most\n");
  printf("3/Hegemonic, I will spread my empire larger\n");
  printf("4/Hybrid, I am up for a challenge\n");

  int ai;
  scanf("%d",&ai);

  printf("\n");

  printf("Very well, you will be player blue");

  player_t*  player1 = player_create(0,'o',0,19,0);
  player_t*  player2 = player_create(1,'x',19,0,ai);
  player_t* players[2] = {player1,player2};

  fight(players);

  free_players(players);
}

void turnament_mode()
{
  printf("Very well, who should be player 1 ?\n");
  printf("1/Random\n");
  printf("2/Greedy\n");
  printf("3/Hegemonic\n");
  printf("4/Hybrid\n");

  int ai1;
  scanf("%d",&ai1);

  printf("\n");

  printf("Very well, who should be player 2 ?\n");
  printf("1/Random\n");
  printf("2/Greedy\n");
  printf("3/Hegemonic\n");
  printf("4/Hybrid\n");

  int ai2;
  scanf("%d",&ai2);

  printf("\n");

  player_t*  player1 = player_create(0,'o',0,19,ai1);
  player_t*  player2 = player_create(1,'x',19,0,ai2);
  player_t* players[2] = {player1,player2};

  int scores[2]={0};
  turnament(players, 100, scores);
  printf("Score of Player 1 : %d\n", scores[0]);
  printf("Score of Player 2 : %d\n", scores[1]);

  free_players(players);
}

/** Program entry point */
int main(void)
{
    printf("\n\nWelcome to the 7 wonders of the world of the 7 colors \n");
    printf("*****************************************************\n\n");
    printf("What game do you want to play ?\n");
    printf("1/ Quick match mode\n");
    printf("2/ AI turnament mode\n");

    int mode;
    scanf("%d", &mode);

    printf("\n");
    printf("\n");

    if(mode==1)
      quick_match_mode();
    else
      turnament_mode();


    return 0; // Everything went well
}
