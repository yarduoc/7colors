all: 7colors

7colors: 7colors.o game_board.o player.o couple_stack.o couple.o
	gcc couple_stack.o couple.o game_board.o player.o 7colors.o -o 7colors

7colors.o: 7colors.c game_board.h player.h
	gcc -Wall -Werror -Wextra -Wno-unused-function -Wno-unused-parameter 7colors.c -c -g

player.o: player.c player.h
	gcc -Wall -Werror -Wextra -Wno-unused-function -Wno-unused-parameter player.c -c -g

game_board.o: game_board.c game_board.h couple_stack.h couple.h
	gcc -Wall -Werror -Wextra -Wno-unused-function -Wno-unused-parameter game_board.c -c -g

couple_stack.o: couple_stack.c couple_stack.h couple.h
	gcc -Wall -Werror -Wextra -Wno-unused-function -Wno-unused-parameter couple_stack.c -c -g

couple.o: couple.c couple.h
	gcc -Wall -Werror -Wextra -Wno-unused-function -Wno-unused-parameter couple.c -c -g
