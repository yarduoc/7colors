/*******************************************************
Name .......... : couple.c
Goal .......... : Giving a simple couple of int structure
Author ........ : Alex Coudray
Date .......... : 11/01/2018    (format MM/DD/YY)
********************************************************/

#include "couple.h"
#include <stdlib.h>
#include <stdio.h>

// Structure definition
struct couple
{
    int x, y;
};

// Constructor
couple_t* make_couple(int x, int y)
{
    couple_t* c = malloc(sizeof(couple_t));
    c -> x = x;
    c -> y = y;
    return c;
}

// Destructor
void free_couple(couple_t* c)
{
    free(c);
}

// Getters for x and y
int get_x( couple_t* c)
{
    return c -> x;
}

int get_y( couple_t* c)
{
    return c -> y;
}
